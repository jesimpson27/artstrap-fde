#!/bin/bash



### init
clear
log="chroot.log"
[ -f "$log" ] && rm "$log"
disk="$(cat /root/disk.tmp)"



### timezone
echo "set time zone..."
while :; do
	find /usr/share/zoneinfo/US -type f -printf "%f\n" | sort | nl -w2 -s': '
	echo "-"
	read -r -p 'selection: ' num
	zone="$(find /usr/share/zoneinfo/US -type f -printf "%f\n" | sort | head -n"$num" 2>> /dev/null | tail -n1)"

	if [ -n "$zone" ]; then
		ln -sf /usr/share/zoneinfo/US/"$zone" /etc/localtime
		break
	fi
	sleep 1
	clear
	echo "try again:"
	echo "-"
done
echo "set time zone...ok"

printf "running \`hwclock\` to generate \'/etc/adjtime\'..."
if ! hwclock --systohc; then
	printf "\rrunning \`hwclock\` to generate \'/etc/adjtime\'...failed\n"; 
	exit 1
fi
printf "\rrunning \`hwclock\` to generate \'/etc/adjtime\'...ok\n"



### localization
printf "editing \'/etc/locale.gen\'..."
if ! sed -i 's/#en_US.UTF-8\ UTF-8/en_US.UTF-8\ UTF-8/g' /etc/locale.gen; then
	printf "\rediting \'/etc/locale.gen\'...failed\n"
	exit 1
fi
printf "\rediting \'/etc/locale.gen\'...ok\n"

printf "generating locales..."
if ! locale-gen > /dev/null 2>> "$log"; then
	printf "\rgenerating locales...failed\n"
	exit 1
fi
printf "\rgenerating locales...ok\n"

printf "creating \'/etc/locale.conf\'..."
echo "LANG=en_US.UTF-8" > /etc/locale.conf
printf "\rcreating \'/etc/locale.conf\'...ok\n"



### network configuration
while [ -z "$hostname" ]; do
	echo "-"
	read -r -p 'enter your hostname: ' hostname
done

printf "setting hostname..."
echo "$hostname" > /etc/hostname
printf "hostname=\'%s\'" "$hostname" > /etc/conf.d/hostname
printf "\rsetting hostname...ok\n"



### initramfs
echo "configuring mkinitcpio..."
# creating a new initramfs is usually not required, because mkinitcpio was run on installation of the kernel package with pacstrap
# add the encrypt and lvm2 hooks to mkinitcpio.conf
sed -i "s/block/block encrypt lvm2/g" /etc/mkinitcpio.conf
# a random-byte file can be generated in order to serve as key to automatically decrypt the system partition during boot by grub, which looks for /crypto_keyfile.bin file by default
dd if=/dev/random of=/crypto_keyfile.bin bs=512 count=8 iflag=fullblock
chmod 000 /crypto_keyfile.bin
sed -i "s/FILES=(/FILES=(\/crypto_keyfile.bin/g" /etc/mkinitcpio.conf
# the newly created key can be added
while ! cryptsetup luksAddKey "$disk" /crypto_keyfile.bin 2>> "$log"; do
	sleep 1
done
# regenerate the initramfs
if ! mkinitcpio -p linux-hardened 2>> "$log"; then
	echo "configuring mkinitcpio...failed"
	exit 1
fi
echo "configuring mkinitcpio...ok"



### root password
echo "set the root password..."
echo "-"
while ! passwd; do
	sleep 1
done
echo "set the root password...ok"



### add user
echo "-"
while [ -z "$username" ]; do
	read -r -p 'enter your username: ' username
done

printf "add a user..."
# video for light utility to change backlight brightness
useradd -m -g users -G users,audio,video -s /bin/bash "$username"
printf "\radd a user...ok\n"

echo "set the user password..."
echo "-"
while ! passwd "$username"; do
	sleep 1
done
echo "set the user password...ok"



### allow user to gain full root privileges when they precede a command with sudo
echo "-"
read -r -p "add user '$username' to sudoers? [Y/n]: " num
if [[ "$num" == [Yy]* ]]; then
	echo "$username ALL=(ALL) ALL" | EDITOR='tee -a' visudo > /dev/null
fi



### bootloader
echo "installing bootloader..."
# in order to unlock the encrypted root partition at boot, the following kernel parameter needs to be set by the boot loader
# cryptdevice=UUID=device-UUID:cryptlvm root=/dev/grouplvm/root
sed -i "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=$(blkid -s UUID -o value "$disk"):cryptlvm root=\/dev\/grouplvm\/root\"/g" /etc/default/grub
sed -i "s/#GRUB_ENABLE_CRYPTODISK=y/GRUB_ENABLE_CRYPTODISK=y/g" /etc/default/grub

if ! grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=grub; then
	echo "installing bootloader...failed"
	exit 1
fi
if ! grub-mkconfig -o /boot/grub/grub.cfg; then
	echo "installing bootloader...failed"
	exit 1
fi
echo "installing bootloader...ok"



### cleanup
mv /root/wifi.sh /home/"$username"
rm /root/disk.tmp
[ -f "$log" ] && rm "$log"
rm -- "$0"
