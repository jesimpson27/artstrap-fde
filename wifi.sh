#!/bin/bash

nmcli -g SSID device wifi list | sort -u | awk 'NF' | nl -w2 -s': ' | tee ssid.tmp
echo "-"
read -r -p 'selection: ' num
ssid="$(head -n"$num" ssid.tmp | tail -n1 | cut -f3 -d' ')"
rm ssid.tmp
read -r -p 'password: ' password
nmcli device wifi connect "$ssid" password "$password"
