#!/bin/bash

### disable secure boot
### UEFI only in thinkpad BIOS
### login
# localhost login: root
# Password: artix



### init
clear
log="install.log"
[ -f "$log" ] && rm "$log"
if ! [ "$(pwd)" = "/root" ]; then
	echo "script must be placed in '/root' directory"
	exit 1
fi
if ! [ -e /root/chroot.sh ]; then
	echo "file '/root/chroot.sh' does not exist"
	exit 1
fi
if ! [ -e /root/wifi.sh ]; then
	echo "file '/root/wifi.sh' does not exist"
	exit 1
fi



### disable pc speaker
rmmod pcspkr 2>> "$log"
rmmod snd_pcsp 2>> "$log"



### connect to the internet
printf "connecting to internet..."
ping -c 1  archlinux.org > /dev/null 2>&1
if ping -c 1 archlinux.org > /dev/null 2>&1; then
	printf "\rconnecting to internet...ok\n"
else
	if ! ip link set wlan0 up 2>> "$log"; then
		echo "connecting to internet...failed"
		exit 1
	fi
	echo
	iw dev wlan0 scan | grep SSID: | cut -f2 -d' ' | grep . | sort | nl -w2 -s': ' | tee ssid.tmp
	echo "-"
	read -r -p 'selection: ' num
	ssid="$(head -n"$num" ssid.tmp | tail -n1 | cut -f3 -d' ')"
	rm ssid.tmp
	read -r -p 'password: ' password
	wpa_supplicant -B -i wlan0 -c <(wpa_passphrase "$ssid" "$password")
	dhcpcd
	if ! ping -c 1 archlinux.org > /dev/null 2>&1; then
		echo "connecting to internet...failed"
		exit 1
	fi
	echo "connecting to internet...ok"
fi



### install packages required for script
echo "installing \`parted\`..."
if ! pacman -Sy --noconfirm --needed parted; then
	echo "installing \`parted\`...failed"
	exit 1
fi
echo "installing \`parted\`...ok"



### update the system clock
printf "updating the system clock..."
if ! rc-service ntpd start > /dev/null 2>> "$log"; then
	printf "\rupdating the system clock...failed\n"
	exit 1
fi
printf "\rupdating the system clock...ok\n"



### partition the disks
echo "partitioning the disks..."
# lsblk -dno NAME,SIZE | grep -vE 'loop|sdb' | xargs -n2 | nl -w2 -s': '
lsblk -dno NAME,SIZE | xargs -n2 | nl -w2 -s': '
echo "-"
read -r -p 'selection: ' num
disk="$(lsblk -dno NAME,SIZE | grep -vE 'loop|sdb' | xargs -n2 | nl -w2 -s': ' | head -n"$num" | tail -n1 | cut -f3 -d' ')"

if [ -z "$disk" ]; then
	echo "no disk selected"
	exit 1
fi

if ! parted --script /dev/"$disk" \
	mklabel gpt \
	mkpart primary 1MiB 513MiB \
	set 1 boot on \
	-- mkpart primary 513MiB -1MiB \
	set 2 lvm on \
	2>> "$log"; then

	echo "partitioning the disks...failed"
	exit 1
fi
echo "partitioning the disks...ok"



### format the partitions
echo "formatting the partitions..."
# sdX partition names different from nvme; modify commands as necessary
if echo "$disk" | grep -q nvme; then
	p="p"
fi

if ! mkfs.fat -F 32 /dev/"$disk""$p"1 > /dev/null 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi

# luks shit
# force loading the linux kernel modules related to serpent and other strong encryptions from your livecd/liveusb
echo "-"
read -r -p 'select swap space (e.g. 16G): ' num
cryptsetup benchmark
if ! cryptsetup --type luks1 luksFormat /dev/"$disk""$p"2 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
if ! cryptsetup open /dev/"$disk""$p"2 cryptlvm 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
if ! pvcreate /dev/mapper/cryptlvm 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
if ! vgcreate grouplvm /dev/mapper/cryptlvm 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
if ! lvcreate -L "$num" grouplvm -n swap 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
if ! lvcreate -l 100%FREE grouplvm -n root 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
# if a logical volume will be formatted with ext4, leave at least 256 mib free space in the volume group to allow using e2scrub 
if ! lvreduce -y -L -256M grouplvm/root 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
if ! mkfs.ext4 /dev/grouplvm/root 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
if ! mkswap /dev/grouplvm/swap 2>> "$log"; then
	echo "formatting the partitions...failed"
	exit 1
fi
echo "formatting the partitions...ok"



### mount the file systems
printf "mounting the file systems..."
if ! mount /dev/grouplvm/root /mnt 2>> "$log"; then
	printf "\rmounting the file systems...failed\n"
	exit 1
fi

if ! swapon /dev/grouplvm/swap 2>> "$log"; then
	printf "\rmounting the file systems...failed\n"
	exit 1
fi

if ! mount --mkdir /dev/"$disk""$p"1 /mnt/boot/efi 2>> "$log"; then
	printf "\rmounting the file systems...failed\n"
	exit 1
fi
printf "\rmounting the file systems...ok\n"



### install base system
echo "installing base system..."
if ! basestrap /mnt \
	base \
	base-devel \
	cryptsetup \
	cryptsetup-openrc \
	efibootmgr \
	elogind-openrc \
	git \
	grub \
	linux-firmware \
	linux-hardened \
	linux-hardened-headers \
	lvm2 \
	lvm2-openrc \
	lynx \
	neovim \
	networkmanager-openrc \
	openrc \
	os-prober \
	2>> "$log"; then
	echo "installing base system...failed"
	exit 1
fi
echo "running pacstrap...ok"



### fstab
printf "generating fstab..."
if ! fstabgen -U /mnt >> /mnt/etc/fstab; then
	printf "\rgenerating fstab...failed\n"
	exit 1
fi
# tmpfs is a temporary filesystem that resides in memory or swap partitions; without systemd, only the /run directory uses tmpfs by default
echo "tmpfs	/tmp	tmpfs	rw,nosuid,noatime,nodev,size=4G,mode=1777	0 0" >> /mnt/etc/fstab
printf "\rgenerating fstab...ok\n"



### prepare for chroot
printf "copying files to new system..."
if ! cp /root/chroot.sh  /mnt/root/; then
	 printf "\rcopying files to new system...failed\n"; 
	 exit 1
fi
if ! cp /root/wifi.sh  /mnt/root/; then
	 printf "\rcopying files to new system...failed\n"; 
	 exit 1
fi
# disk tempfile for chroot.sh to use
printf "/dev/%s%s2" "$disk" "$p" > /mnt/root/disk.tmp
printf "\rcopying files to new system...ok\n"


### chroot
echo "changing root into new system. bye! :("
if ! artix-chroot /mnt /root/chroot.sh; then
	echo "something went wrong in chroot environment; check logfile at '/mnt/root/chroot.log'"
	exit 1
fi


### reboot and cleanup
read -r -p 'base installation complete! reboot now? [Y/n]: ' selection
if [[ "$selection" == [Yy]* ]]; then
	reboot
fi
